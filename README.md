# SpyZer

## WTF

The idea of the whole thing is to make a web inspector which inspects the
*whole* Web and takes data from it.

The idea is to use big guy's tools against them.

With that in mind imagine you are able to browse all the websites in the world
(or at least a significant portion of them) and check how they are exploiting
the user privacy, sending tons of unwanted data and all of that shit.

We are able to do it.

Keep reading.

## Features

- A headless browser for JS load time analysis and network analysis
- A list of seed URLs, but spider-style crawling. Reads links and jumps to
  them.
- Data visualization part. Visualize all the data like it was Friday. **Make
  people understand**.

## How to

### Performance/network analysis

There are many tools for this, many browsers have `headless` modes for
performance and network analysis. There are also some headless browsers like
[PhantomJS](http://phantomjs.org/) or tools like
[Selenium](http://docs.seleniumhq.org/) who were created to test websites in CI
systems.

The data obtained from the browser can be stored in [HAR
files](https://en.wikipedia.org/wiki/.har).

Our friends at [ind.ie](http://ind.ie) made something similar with [the
inspector](https://source.ind.ie/better/inspector) they made for
[better](http://better.fyi).

> We can start downloading their data: https://archive.better.fyi

**Maybe use Clojure** for all this thing? [We can do it
too](https://github.com/igrishaev/etaoin)

### Web crawling

We have to go deeper than ind.ie example explained in before because they focus
on a manual analysis and we are too lazy for that.

They also have to limit the amount of websites they check (obviously) to [the
alexa top 5000](https://source.ind.ie/better/domains).

Let's use spiders and get tons of domains to check!

We can use any spider for this... [scrapy](https://scrapy.org/) looks cool but
there are more tools for this. Like this [beast made in
Java](https://wiki.apache.org/nutch/FrontPage#What_is_Apache_Nutch.3F).

**Maybe use Clojure** for all this thing? [We can do it
too](https://github.com/shriphani/pegasus)

### Data exploitation

We know some big-data, right?

Let's use it.

Once we collect the HAR files (or more data maybe) we can use some analysis on
top of it. It shouldn't be very difficult.

The idea of this step is to create connections and infer metrics that we can
show in a cool panel.

I don't know if we'll need to make it in a cluster or something but we can run
[spark](https://spark.apache.org/) or something else.

You said ML? It's also welcome.

### Data visualization

This is the **real point** of the whole thang.  
People don't give a fuck if they don't understand.

Make animations, interactive displays, fuck with the scroll and make it work in
a non-standard way and all that. Make all the dirty tricks the modern and
classy websites do.

Use [d3](https://d3js.org/), or maybe don't. But visit the website to
understand what I'm talking about.
